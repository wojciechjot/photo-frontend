import Vue from 'vue'
import Router from 'vue-router'
import Start from './views/Start.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'start',
            component: Start
        },
        {
            path: '/offer',
            name: 'offer',
            component: () => import('./views/Offer.vue')
        },
        {
            path: '/gallery',
            name: 'gallery',
            component: () => import('./views/Gallery.vue')
        },
        {
            path: '/contact',
            name: 'contact',
            component: () => import('./views/Contact.vue')
        }
    ]
})
